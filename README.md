# BubbleTab

#### 项目介绍
- 项目名称：BubbleTab
- 所属系列：openharmony的第三方组件适配移植
- 功能：提供一个气泡风格的tab栏 
- 项目移植状态：已完成主功能
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1(实时更新以当前最新版本为准)
- 基线版本：Releases v1.0.2


#### 效果演示

![screen](video.gif)

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies { 
    implementation'com.gitee.chinasoft_ohos:bubbletab:1.0.0'
    ......  
 }
```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
#### 使用说明

  1.在xml中直接引用控件

```xml
<com.github.florent37.library.BubbleTab
        ohos:id="$+id:customerDependentLayout"
        ohos:height="50vp"
        ohos:width="match_parent"
        ohos:background_element="$color:white"
        ohos:orientation="horizontal"
        app:CustomerDependentLayout_circleColor="#e64280"
        app:CustomerDependentLayout_circleRatio="1.9">

        <DependentLayout
            ohos:id="$+id:image_container"
            ohos:height="match_parent"
            ohos:width="0vp"
            ohos:weight="1">

            <Image
                ohos:height="26vp"
                ohos:width="26vp"
                ohos:background_element="$graphic:bubbletab_ic_hourglass_selector"
                ohos:center_in_parent="true" />

        </DependentLayout>

        <DependentLayout
            ohos:id="$+id:image2_container"
            ohos:height="match_parent"
            ohos:width="0vp"
            ohos:weight="1">

            <Image
                ohos:height="26vp"
                ohos:width="26vp"
                ohos:background_element="$graphic:bubbletab_ic_event_selector"
                ohos:center_in_parent="true" />

        </DependentLayout>

        <DependentLayout
            ohos:id="$+id:image3_container"
            ohos:height="match_parent"
            ohos:width="0vp"
            ohos:weight="1">

            <Image
                ohos:height="26vp"
                ohos:width="26vp"
                ohos:background_element="$graphic:bubbletab_ic_query_selector"
                ohos:center_in_parent="true" />
        </DependentLayout>

        <DependentLayout
            ohos:id="$+id:image4_container"
            ohos:height="match_parent"
            ohos:width="0vp"
            ohos:weight="1">

            <Image
                ohos:id="$+id:image4"
                ohos:height="26vp"
                ohos:width="26vp"
                ohos:background_element="$graphic:bubbletab_ic_search_selector"
                ohos:center_in_parent="true" />
        </DependentLayout>

        <DependentLayout
            ohos:id="$+id:image5_container"
            ohos:height="match_parent"
            ohos:width="0vp"
            ohos:weight="1">

            <Image
                ohos:id="$+id:image5"
                ohos:height="26vp"
                ohos:width="26vp"
                ohos:background_element="$graphic:bubbletab_ic_home_selector"
                ohos:center_in_parent="true" />
        </DependentLayout>
    </com.github.florent37.library.BubbleTab>

    <PageSlider
        ohos:id="$+id:rootPage"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:background_element="$color:white"
        ohos:top_margin="6vp"
        />
```

2.在java中绑定对应的pageSlider,设置相应item选中接口

```java
bubbleTab.setupWithViewPager(pageSlider);

 bubbleTab.setOnItemSelectedListener(new BubbleTab.ItemSelectedListener() {
            @Override
            public void onItemSelected(int index, boolean isSelected) {
                mSelPos = index;
                if (mItemList == null) {
                    mItemList = bubbleTab.getItemComponent();
                }
                ComponentContainer componentContainer = (ComponentContainer) mItemList.get(index);
                if (componentContainer != null) {
                    Image image = (Image) componentContainer.getComponentAt(0);
                    image.setSelected(isSelected);
                }
            }

        });
```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常   

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0


#### 版权和许可信息
```
   Copyright 2016 florent37, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```


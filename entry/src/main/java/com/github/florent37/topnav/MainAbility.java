/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.github.florent37.topnav;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.PageSlider;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.utils.Color;
import ohos.bundle.AbilityInfo;


import com.github.florent37.library.BubbleTab;

import java.util.ArrayList;
import java.util.List;

/**
 * MainAbility 主界面
 *
 * @since 2021-04-16
 */
public class MainAbility extends Ability {
    private List<Component> mItemList;
    private PageSlider pageSlider;
    private List<AbstractTabComponent> tabComponents;
    private BubbleTab bubbleTab;
    private int mSelPos;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().setStatusBarColor(Color.getIntColor("#e64280"));
        initPages();

    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        initPages();
        if (displayOrientation.ordinal() == AbilityInfo.DisplayOrientation.PORTRAIT.ordinal()) {

        } else if (displayOrientation.ordinal() == AbilityInfo.DisplayOrientation.LANDSCAPE.ordinal()) {

        }
    }

    private void initPages() {
        // 从根页面布局的xml文件中获取到PageSlider
        Component tempComponent = findComponentById(ResourceTable.Id_rootPage);
        if (tempComponent == null) {
            return;
        }
        pageSlider = (PageSlider) tempComponent;
        bubbleTab = (BubbleTab) findComponentById(ResourceTable.Id_customerDependentLayout);

        // 为PageSlider添加显示数据，其中PageProvider是自定义的类，继承自PageSliderProvider
        tabComponents = new ArrayList<>();
        CustomListContainer testTabComponent1 = new CustomListContainer(this, null);
        CustomListContainer testTabComponent2 = new CustomListContainer(this, null);
        CustomListContainer testTabComponent3 = new CustomListContainer(this, null);
        CustomListContainer testTabComponent4 = new CustomListContainer(this, null);
        CustomListContainer testTabComponent5 = new CustomListContainer(this, null);
        tabComponents.add(testTabComponent1);
        tabComponents.add(testTabComponent2);
        tabComponents.add(testTabComponent3);
        tabComponents.add(testTabComponent4);
        tabComponents.add(testTabComponent5);
        pageSlider.setProvider(new PageProvider(this, tabComponents));
        pageSlider.setSlidingPossible(true);
        bubbleTab.setOnItemSelectedListener(new BubbleTab.ItemSelectedListener() {
            @Override
            public void onItemSelected(int index, boolean isSelected) {
                mSelPos = index;
                if (mItemList == null) {
                    mItemList = bubbleTab.getItemComponent();
                }
                ComponentContainer componentContainer = (ComponentContainer) mItemList.get(index);
                if (componentContainer != null) {
                    Image image = (Image) componentContainer.getComponentAt(0);
                    image.setSelected(isSelected);
                }
            }

        });
        bubbleTab.setupWithPageSlider(pageSlider);
        // 须放最后,不然横竖屏切换后不能回到之前选中item
        bubbleTab.setInit(mSelPos);
    }

    /**
     * PageProvider pageSlider 适配器
     *
     * @since 2021-04-16
     */
   static class PageProvider extends PageSliderProvider {
        private static final String TAG = "Information";
        private List<AbstractTabComponent> tabComponents;

        /**
         * PageProvider construction
         *
         * @param slice
         * @param tabComponents
         */
        public PageProvider(MainAbility slice, List<AbstractTabComponent> tabComponents) {
            if (tabComponents != null) {
                this.tabComponents = tabComponents;
            } else {
                this.tabComponents = new ArrayList<>();
            }
        }

        @Override
        public int getCount() {
            return tabComponents.size();
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int position) {
            Component component = null;
            if (componentContainer == null) {
                return component;
            }
            if (0 <= position && position < tabComponents.size()) {
                component = tabComponents.get(position).getRootComponent();
            }
            componentContainer.addComponent(component);
            return component;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object obj) {
            if (componentContainer == null) {
                return;
            }
            if (obj instanceof Component) {
                componentContainer.removeComponent((Component) obj);
            }
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object o) {
            return true;
        }
    }
}

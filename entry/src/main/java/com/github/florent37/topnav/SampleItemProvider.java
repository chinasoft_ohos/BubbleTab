/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.github.florent37.topnav;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;


import com.github.florent37.library.Utils;

import java.util.List;

/**
 * pageSlide数据适配器
 *
 * @author HUAWEI
 * @since 2021-04-16
 */
public class SampleItemProvider extends BaseItemProvider {
    private static final float ITEM_VIEW_WIDTH_RATIO = 0.5f;
    private List<SampleItem> list;
    private Context context;
    private final int displayWidth;

    /**
     * SimpleSearchView constructor
     *
     * @param list 数据集合
     * @param context
     */
    public SampleItemProvider(List<SampleItem> list, Context context) {

        this.list = list;
        this.context = context;
        displayWidth = Utils.getDisplayWidthInPx(context);
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        if (list != null && position >= 0 && position < list.size()) {
            return list.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Holder holder;
        if (component == null) {
            component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_sample, null, true);
            holder = new Holder();
            holder.component1 = component.findComponentById(ResourceTable.Id_item_index1);
            holder.component2 = component.findComponentById(ResourceTable.Id_item_index2);
            component.setTag(holder);
        } else {
            holder = (Holder) component.getTag();
        }
        component.setWidth(displayWidth);
        holder.component1.setWidth((int) (displayWidth * ITEM_VIEW_WIDTH_RATIO));
        holder.component2.setWidth((int) (displayWidth * ITEM_VIEW_WIDTH_RATIO));
        return component;
    }

    /**
     * Holder
     *
     * @since 2021-04-27
     */
    public static class Holder {
        Component component1;
        Component component2;
    }


}
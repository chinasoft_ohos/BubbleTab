/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.florent37.topnav;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * ListContainer view
 *
 * @author HUAWEI
 * @since 2021-04-16
 */
public class CustomListContainer extends ListContainer implements AbstractTabComponent {
    private static final int CHILD_VIEW_COUNT = 6;

    /**
     * CustomListContainer constructor
     *
     * @param context
     */
    public CustomListContainer(Context context) {
        super(context);
        init(context, null);
    }

    /**
     * CustomListContainer constructor
     *
     * @param context
     * @param attrSet
     */
    public CustomListContainer(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    /**
     * CustomListContainer constructor
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public CustomListContainer(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }


    private void init(Context context, AttrSet attrSet) {
        setLongClickable(false);
        List<SampleItem> list = getData();
        SampleItemProvider sampleItemProvider = new SampleItemProvider(list, context);
        setItemProvider(sampleItemProvider);
    }


    private ArrayList<SampleItem> getData() {
        ArrayList<SampleItem> list = new ArrayList<>();
        for (int temp = 0; temp <= CHILD_VIEW_COUNT; temp++) {
            list.add(new SampleItem("Item" + temp));
        }
        return list;
    }

    @Override
    public Component getRootComponent() {
        return this;
    }


}

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.florent37.library;

import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;

/**
 * AttrUtil 自定义属性相关处理工具类
 *
 * @author HUAWEI
 * @since 2021-04-27
 */
public class AttrUtil {


    /**
     * 获取自定义属性是string值
     *
     * @param attrSet
     * @param key 自定义属性名称
     * @param defValue 默认值
     * @return xml配置的值
     */
    public static String getStringValue(AttrSet attrSet, String key, String defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getStringValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义属性是颜色值
     *
     * @param attrSet
     * @param key 自定义属性名称
     * @param defValue 默认值
     * @return xml配置的值
     */
    public static Color getColorValue(AttrSet attrSet, String key, String defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return new Color(Color.getIntColor(attrSet.getAttr(key).get().getStringValue()));
        } else {
            return new Color(Color.getIntColor(defValue));
        }
    }

    /**
     * 获取自定义属性是颜色值
     *
     * @param attrSet
     * @param key 自定义属性名称
     * @param defValue 默认值
     * @return xml配置color对应int值
     */
    public static int getColorIntValue(AttrSet attrSet, String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getColorValue().getValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义属性是布尔值
     *
     * @param attrSet
     * @param key 自定义属性名称
     * @param defValue 默认值
     * @return xml配置值
     */

    public static Boolean getBooleanValue(AttrSet attrSet, String key, boolean defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getBoolValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义属性是带单位的数字
     *
     * @param attrSet
     * @param key 自定义属性名称
     * @param defDimensionValue 默认值
     * @return xml配置值
     */
    public static int getDimension(AttrSet attrSet, String key, int defDimensionValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getDimensionValue();
        } else {
            //默认数据转换为vp
            return defDimensionValue;
        }
    }

    /**
     * 获取自定义属性int值
     *
     * @param attrSet
     * @param key 自定义属性名称
     * @param defValue 默认值
     * @return xml配置值
     */
    public static Integer getIntegerValue(AttrSet attrSet, String key, Integer defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义属性float值
     *
     * @param attrSet
     * @param key 自定义属性名称
     * @param defValue 默认值
     * @return xml配置值
     */
    public static float getFloatValue(AttrSet attrSet, String key, float defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getFloatValue();
        } else {
            return defValue;
        }
    }


}
